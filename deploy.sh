#!/usr/bin/env bash
apk upgrade --no-cache
apk add --update curl

# Deploy to Docker Hub
docker pull psono-docker.jfrog.io/psono/psono-combo:latest
docker tag psono-docker.jfrog.io/psono/psono-combo:latest psono/psono-combo:latest
docker push psono/psono-combo:latest

## Inform production stage about new image
curl -X POST https://hooks.microbadger.com/images/psono/psono-combo/xGR9qDDEXg30xqJcKPEIpCsZOSc= || true
#curl -X POST $psono_image_updater_url
#
## Deploy to GitHub
#echo "Cloning gitlab.com/psono/psono-combo.git"
#git clone https://gitlab.com/psono/psono-combo.git
#cd psono-combo
#git branch --track develop origin/develop
#git fetch --all
#git pull --all
#
#echo "Empty .ssh folder"
#if [ -d "/root/.ssh" ]; then
#    rm -Rf /root/.ssh;
#fi
#mkdir -p /root/.ssh
#
#echo "Fill .ssh folder"
#echo "$github_deploy_key" > /root/.ssh/id_rsa
#cat > /root/.ssh/known_hosts <<- "EOF"
#|1|QihaxuxIU4rUFjd+Zi5Mr3V0oyI=|m1minLYaqd2pSUN52YJk1ROukfY= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
#|1|dhFyBNrE6k3jSyFFOoEbeJKgbcs=|W0ag0VmyD+G4NSRpMOGkApaY594= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
#EOF
#chmod 600 /root/.ssh/id_rsa
#chmod 600 /root/.ssh/known_hosts
#
#echo "Push to github.com/psono/psono-combo.git"
#git remote set-url origin git@github.com:psono/psono-combo.git
#git push --all origin


echo "Trigger psono pw deployment"
curl -X POST -F token=$PSONO_COMBO_UPDATED_TOKEN -F ref=master https://gitlab.com/api/v4/projects/24874262/trigger/pipeline
