if [ ! -z "$PSONO_WEBCLIENT_CONFIG_JSON" ]
then
      echo "$PSONO_WEBCLIENT_CONFIG_JSON" > /usr/share/nginx/html/config.json
fi
if [ ! -z "$PSONO_PORTAL_CONFIG_JSON" ]
then
      echo "$PSONO_PORTAL_CONFIG_JSON" > /usr/share/nginx/html/portal/config.json
fi

python3 /root/combo/createconfig.py /root/combo/templates/nginx.conf.tpl /etc/nginx/nginx.conf && \
python3 /root/combo/createconfig.py /root/combo/templates/default.conf.tpl /etc/nginx/conf.d/default.conf && \
python3 /root/combo/createconfig.py /root/configs/docker/psono_uwsgi_port.ini.tpl /root/configs/docker/psono_uwsgi_port.ini && \
python3 /root/psono/manage.py migrate && \
/bin/sh -c "uwsgi --ini /root/configs/docker/psono_uwsgi_port.ini &" && \
nginx -g 'daemon off;'
