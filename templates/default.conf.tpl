upstream upstream-localhost-8000 {
    server localhost:8000 max_fails=0 fail_timeout=0;
}
server {
    listen 80 default_server;

    client_max_body_size 256m;

    {% if NGINX_STRICT_TRANSPORT_SECURITY %}
    add_header Strict-Transport-Security {{ NGINX_STRICT_TRANSPORT_SECURITY | safe }};
    {% endif %}

    {% if NGINX_HEADER_REFERRER_POLICY %}
    add_header Referrer-Policy {{ NGINX_HEADER_REFERRER_POLICY | safe }};
    {% endif %}

    {% if NGINX_HEADER_X_FRAME_OPTIONS %}
    add_header X-Frame-Options {{ NGINX_HEADER_X_FRAME_OPTIONS | safe }};
    {% endif %}

    {% if NGINX_HEADER_X_CONTENT_TYPE_OPTIONS %}
    add_header X-Content-Type-Options {{ NGINX_HEADER_X_CONTENT_TYPE_OPTIONS | safe }};
    {% endif %}

    {% if NGINX_HEADER_X_XSS_PROTECTION %}
    add_header X-XSS-Protection {{ NGINX_HEADER_X_XSS_PROTECTION | safe }};
    {% endif %}

    {% if NGINX_HEADER_CONTENT_SECURITY_POLICY %}
    add_header Content-Security-Policy {{ NGINX_HEADER_CONTENT_SECURITY_POLICY | safe }};
    {% endif %}

    gzip on;
    gzip_disable "msie6";

    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_min_length 256;
    gzip_types text/plain text/css application/json application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;

    root /usr/share/nginx/html;

    location /server {
        rewrite ^/server/(.*) /$1 break;
        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;

        add_header Last-Modified $date_gmt;
        add_header Pragma "no-cache";
        add_header Cache-Control "private, max-age=0, no-cache, no-store";
        if_modified_since off;
        expires off;
        etag off;

        proxy_pass          http://upstream-localhost-8000;
    }

    location ~* ^/portal.*\.(?:ico|css|js|gif|jpe?g|png|eot|woff|woff2|ttf|svg|otf)$ {
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control "public";
    }

    location ~* \.(?:ico|css|js|gif|jpe?g|png|eot|woff|woff2|ttf|svg|otf)$ {
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control "public";
    }

    location /portal {
        alias   /usr/share/nginx/html/portal;
        index  index.html index.htm;
        try_files $uri /portal/index.html;  # forward all requests to index.html
    }

    # location / {
    #     root   /usr/share/nginx/html;
    #     index  index.html index.htm;
    # }
    #
    # error_page   500 502 503 504  /50x.html;
    # location = /50x.html {
    #     root   /usr/share/nginx/html;
    # }
}
