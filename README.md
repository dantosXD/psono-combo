# PSONO Combo - Password Manager

[![build status](https://gitlab.com/psono/psono-combo/badges/master/pipeline.svg)](https://gitlab.com/psono/psono-combo/commits/master) [![build status](https://img.shields.io/microbadger/layers/psono/psono-combo.svg)](https://hub.docker.com/r/psono/psono-combo/) [![build status](https://img.shields.io/docker/image-size/psono/psono-combo.svg)](https://hub.docker.com/r/psono/psono-combo/) [![build status](https://img.shields.io/docker/pulls/psono/psono-combo.svg)](https://hub.docker.com/r/psono/psono-combo/)

# Canonical source

The canonical source of PSONO Combo is [hosted on GitLab.com](https://gitlab.com/psono/psono-combo).

# Documentation

The documentation for the psono combo can be found here:

[Psono Documentation](https://doc.psono.com/)

## LICENSE

Visit the [License.md](/LICENSE.md) for more details
