FROM psono/psono-server:latest

LABEL maintainer="Sascha Pfeiffer <sascha.pfeiffer@psono.com>"
COPY cmd.sh /root/configs/docker/cmd.sh
COPY ./templates /root/combo/templates
COPY ./createconfig.py /root/combo/createconfig.py
WORKDIR /root

RUN apk upgrade --no-cache && \
    mkdir -p /root/.pip && \
    echo '[global]' >> /root/.pip/pip.conf && \
    echo 'index-url = https://psono.jfrog.io/psono/api/pypi/pypi/simple' >> /root/.pip/pip.conf && \
    apk add --no-cache \
        curl \
        zip \
        nginx && \
    curl https://psono.jfrog.io/psono/psono/client/latest/webclient.zip --output webclient.zip && \
    mkdir -p webclient && \
    mkdir -p /usr/share/nginx/ && \
    unzip webclient.zip -d webclient && \
    mv webclient /usr/share/nginx/html && \
    rm webclient.zip && \
    curl https://psono.jfrog.io/psono/psono/admin-client/latest/webclient.zip --output adminwebclient.zip && \
    mkdir -p adminwebclient && \
    unzip adminwebclient.zip -d adminwebclient && \
    mv adminwebclient /usr/share/nginx/html/portal && \
    rm adminwebclient.zip && \
    apk del --no-cache \
        build-base \
        libffi-dev \
        zip \
        linux-headers && \
    rm -Rf \
        /root/.cache \
        /tmp/*


HEALTHCHECK --interval=2m --timeout=3s \
	CMD curl -f http://localhost/server/healthcheck/ || exit 1

EXPOSE 80

CMD ["/bin/sh", "/root/configs/docker/cmd.sh"]